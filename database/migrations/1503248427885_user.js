'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('firstname', 191).notNullable()
      table.string('lastname', 191).notNullable()
      table.string('email', 191).notNullable().unique()
      table.text('address').notNullable()
      table.string('phone', 191).notNullable()
      table.string('postcode', 191).notNullable()
      table.string('city', 191).notNullable()
      table.string('country', 191).notNullable()
      table.integer('user_types_id', 191).notNullable()
      table.string('emailVerifedToken', 191)
      table.string('passwordResetToken', 191)
      table.string('deviceToken', 191)
      table.string('password', 60).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
