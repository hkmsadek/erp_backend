'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SliderSchema extends Schema {
  up () {
    this.create('sliders', (table) => {
      table.increments()
      table.string('url',191).notNullable()
      table.string('link',191).notNullable()
      table.string('header',191).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('sliders')
  }
}

module.exports = SliderSchema
