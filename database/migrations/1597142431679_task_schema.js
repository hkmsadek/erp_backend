'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TaskSchema extends Schema {
  up () {
    this.create('tasks', (table) => {
      table.increments()
      table.integer('projects_id',10).notNullable()
      table.string('status',191).notNullable()
      table.integer('users_id',10).notNullable()
      table.integer('agents_id',10).notNullable()
      table.text('description').notNullable()
      table.integer('duration',4).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('tasks')
  }
}

module.exports = TaskSchema
