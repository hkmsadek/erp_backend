'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RequestSchema extends Schema {
  up () {
    this.create('requests', (table) => {
      table.increments()
      table.integer('projects_id',10).notNullable()
      table.text('description').notNullable()
      table.string('type',191).notNullable()
      table.string('status',191).notNullable()
      table.integer('users_id',10).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('requests')
  }
}

module.exports = RequestSchema
