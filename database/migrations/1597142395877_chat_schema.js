'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChatSchema extends Schema {
  up () {
    this.create('chats', (table) => {
      table.increments()
      table.integer('conversations_id', 10).notNullable()
      table.text('message').notNullable()
      table.integer('attachments_id',10).notNullable()
      table.integer('sender',10).notNullable()
      table.integer('receiver',10).notNullable()
      table.boolean('isDeleted').defaultTo(0)
      table.boolean('isSeen').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('chats')
  }
}

module.exports = ChatSchema
