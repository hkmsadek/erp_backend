'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentTransactionSchema extends Schema {
  up () {
    this.create('payment_transactions', (table) => {
      table.increments()
      table.integer('users_id',10).notNullable()
      table.integer('projects_id',10).notNullable()
      table.string('type',191).notNullable()
      table.decimal('amount',8,2).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('payment_transactions')
  }
}

module.exports = PaymentTransactionSchema
