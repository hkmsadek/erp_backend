'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectEmployeeSchema extends Schema {
  up () {
    this.create('project_employees', (table) => {
      table.increments()
      table.integer('users_id',10).notNullable()
      table.integer('projects_id',10).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('project_employees')
  }
}

module.exports = ProjectEmployeeSchema
