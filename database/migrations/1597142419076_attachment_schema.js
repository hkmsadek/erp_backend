'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AttachmentSchema extends Schema {
  up () {
    this.create('attachments', (table) => {
      table.increments()
      table.integer('projects_id',10).notNullable()
      table.integer('users_id',10).notNullable()
      table.string('type',191).notNullable()
      table.string('extension',191).notNullable()
      table.text('url')
      table.timestamps()
    })
  }

  down () {
    this.drop('attachments')
  }
}

module.exports = AttachmentSchema
