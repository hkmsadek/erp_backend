'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnswerSchema extends Schema {
  up () {
    this.create('answers', (table) => {
      table.increments()
      table.integer('questions_id',10).notNullable()
      table.integer('users_id',10).notNullable()
      table.text('content').notNullable()
      table.boolean('isAnswer').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('answers')
  }
}

module.exports = AnswerSchema
