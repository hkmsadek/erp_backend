'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VideoRequestSchema extends Schema {
  up () {
    this.create('video_requests', (table) => {
      table.increments()
      table.integer('users_id',10).notNullable()
      table.integer('projects_id',10).notNullable()
      table.integer('duration',10).notNullable()
      table.date('date').notNullable()
      table.string('status',191).notNullable()
      table.time('time').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('video_requests')
  }
}

module.exports = VideoRequestSchema
