'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TutorialSchema extends Schema {
  up () {
    this.create('tutorials', (table) => {
      table.increments()
      table.string('title',191).notNullable()
      table.text('description').notNullable()
      table.integer('categories_id',10).notNullable()
      table.string('url',191).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('tutorials')
  }
}

module.exports = TutorialSchema
