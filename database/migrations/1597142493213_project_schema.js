'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.increments()
      table.text('demoFiles').notNullable()
      table.integer('users_id',10).notNullable()
      table.text('description').notNullable()
      table.decimal('price',10,2).notNullable()
      table.integer('hour',10).notNullable()
      table.string('paymentStatus',191).notNullable()
      table.string('status',191).defaultTo('Pending')
      table.timestamps()
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
