'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionSchema extends Schema {
  up () {
    this.create('questions', (table) => {
      table.increments()
      table.integer('projects_id',10).notNullable()
      table.text('content').notNullable()
      table.integer('users_id',10).notNullable()
      table.boolean('isSolved').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('questions')
  }
}

module.exports = QuestionSchema
