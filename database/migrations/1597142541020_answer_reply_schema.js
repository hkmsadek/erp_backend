'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnswerReplySchema extends Schema {
  up () {
    this.create('answer_replies', (table) => {
      table.increments()
      table.integer('answers_id',10).notNullable()
      table.integer('users_id',10).notNullable()
      table.text('content').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('answer_replies')
  }
}

module.exports = AnswerReplySchema
