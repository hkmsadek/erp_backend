'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TutorialCategorySchema extends Schema {
  up () {
    this.create('tutorial_categories', (table) => {
      table.increments()
      table.string('name',191).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('tutorial_categories')
  }
}

module.exports = TutorialCategorySchema
