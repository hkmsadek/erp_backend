'use strict'
const Route = use('Route')
Route.post('/sales/login', 'SaleController.login').middleware(['guest'])
Route.get('/sales/initdata', 'SaleController.initdata').middleware(['SalesRepresentative'])
Route.get('/logout', 'SaleController.logout')

Route.group(() => {
    Route.post('/sales/payroll', 'PaymentTransactionController.salesTransactionById')
    


    // Clients || Projects
    Route.get('/sales/clientList', 'SaleController.clientList')

    Route.get('/sales/projectStatistics/:id', 'SaleController.projectStatistics')

    Route.get('/sales/test', 'SaleController.test')
    // Booking || Appointments
    Route.get('/sales/appintments', 'SaleController.appintments')
    Route.post('/sales/appintments/editStatus', 'SaleController.editAppointmentStatus')
    Route.post('/sales/appintments/add', 'SaleController.addAppointment')


    // Query Routes
    Route.get('/sales/getClientList', 'SaleController.getClientList')


    /**********************  All Chat Routes *****************************/
    // get chat list ( agent )
    Route.get('/sales/messenger/agent/conversation/get/list', 'SaleChatController.getClientConversationList')
    // last person chat chat
    Route.get('/sales/messenger/agent/conversation/get/chatWithLastPerson/:id', 'SaleChatController.getClientChatWithLastPerson')
    // get chat history by id
    Route.get('/sales/messenger/converstaion/:id', 'SaleChatController.getChatHistory')
    // get chat history by username
    Route.get('/sales/messenger/chat/converstaion/:id', 'SaleChatController.getChatHistoryByUserId')
    // get previous message of same chat
    Route.get('/sales/messenger/conversation/previous-chat/:conId/:id', 'SaleChatController.getMoreMessages')
    // insert chat
    Route.post('/sales/messenger/chat/add', 'SaleChatController.insertChat')
    // **************************************************************** ws ******************************************************************************
    Route.post('/sales/messenger/ws/get-last-chat', 'SaleChatController.getLastChatMessageToWS')
    // **************************************************************** ws ******************************************************************************
    /**********************  All Chat Routes *****************************/


    // All Campaine Routes
    Route.get('/sales/getALlCampaignSummary', 'SaleController.getALlCampaignSummary')
    Route.post('/sales/addnewCampaignSubcriber', 'SaleController.addnewCampaignSubcriber')

    //Project
    Route.get('/sales/all_project_tech', 'SaleController.all_project_tech')
    Route.get('/sales/all_project_doc_tech', 'SaleController.all_project_doc_tech')
    Route.get('/sales/pending_project_count_tech', 'SaleController.pending_project_count_tech')
    Route.get('/sales/get_all_project_tech', 'SaleController.get_all_project_tech')
    Route.get('/sales/all_project_timeline_tech', 'SaleController.all_project_timeline_tech')
    Route.get('/sales/running_project_count_tech', 'SaleController.running_project_count_tech')
    Route.get('/sales/get_single_project', 'SaleController.get_single_project')
    Route.post('/sales/projectStatus', 'SaleController.projectStatus')

    //Question
    Route.get('/sales/all_project_ques_tech', 'SaleQuestionController.all_project_ques_tech')
    Route.get('/sales/single_question_tech', 'SaleQuestionController.single_question_tech')
    Route.post('/sales/edit_question_tech', 'SaleQuestionController.edit_question_tech')
    Route.post('/sales/delete_ques_tech', 'SaleQuestionController.delete_ques_tech')
    Route.post('/sales/questionAdd', 'SaleQuestionController.questionAdd')

    //Answer
    Route.get('/sales/answerShow', 'SaleQuestionController.answerShow')
    Route.post('/sales/answerAdd', 'SaleQuestionController.answerAdd')
    Route.post('/sales/edit_ans_tech', 'SaleQuestionController.edit_ans_tech')
    Route.post('/sales/delete_answer_tech', 'SaleQuestionController.delete_answer_tech')

    //Reply
    Route.post('/sales/replyAdd', 'SaleQuestionController.replyAdd') 
    Route.get('/sales/allReplyShow', 'SaleQuestionController.allReplyShow') 
    Route.post('/sales/edit_reply_tech', 'SaleQuestionController.edit_reply_tech') 
    Route.post('/sales/delete_reply_tech', 'SaleQuestionController.delete_reply_tech')

    // Trainging Videos
    Route.get('/sales/showInstructionTutorials', 'SaleController.showInstructionTutorials')
    Route.get('/sales/showInstructionTutorialsById', 'SaleController.showInstructionTutorialsById')



    // Balance 
    Route.get('/sales/getAgentBalance', 'SaleController.getAgentBalance')
    Route.get('/sales/allagentbalance', 'SaleController.allagentbalance')


    // Dashbaord 
    Route.get('/sales/dashboardOverview', 'SaleController.dashboardOverview')

}).middleware(['SalesRepresentative'])