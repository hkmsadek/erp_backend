'use strict'
const Route = use('Route')



// Banner
Route.get('/app/client/get/banner', 'SliderController.getSliderDetails')

// Client
Route.post('/clientlogin', 'ClientController.clientLogin')
Route.post('/register_prospective_client', 'ClientController.clientRegister')
Route.get('/clinet_logout', 'ClientController.clinet_logout')
Route.get('/showSalesReps', 'ClientController.showSalesReps')
// Tutorial
Route.get('/showInstructionTutorials', 'ClientController.showInstructionTutorials')
Route.get('/showInstructionTutorialsById', 'ClientController.showInstructionTutorialsById')

// dashbaord
Route.get('/get_dashboad_project', 'ClientController.get_dashboad_project')

// Chat
// get chat list ( agent )
Route.get('/app/messenger/client/conversation/get/list', 'ClientChatController.getClientConversationList')
// last person chat chat
Route.get('/app/messenger/client/conversation/get/chatWithLastPerson/:id', 'ClientChatController.getClientChatWithLastPerson')
// get chat history by id
Route.get('/app/messenger/converstaion/:id', 'ClientChatController.getChatHistory')
// get chat history by username
Route.get('/app/messenger/chat/converstaion/:id', 'ClientChatController.getChatHistoryByUserId')
// get previous message of same chat
Route.get('/app/messenger/conversation/previous-chat/:conId/:id', 'ClientChatController.getMoreMessages')
// insert chat
Route.post('/app/messenger/chat/add', 'ClientChatController.insertChat')
// **************************************************************** ws ******************************************************************************
Route.post('/app/messenger/ws/get-last-chat', 'ClientChatController.getLastChatMessageToWS')
// **************************************************************** ws ******************************************************************************

// Projects
Route.get('/showProject', 'ClientProjectController.showProject')
Route.get('/showSingleProject', 'ClientProjectController.showSingleProject')
Route.post('/client/addProjectRequest', 'ClientProjectController.addProjectRequest')


//Project
Route.get('/all_project_tech', 'ClientProjectController.all_project_tech')
Route.get('/all_project_doc_tech', 'ClientProjectController.all_project_doc_tech')
Route.post('projectStatus', 'ClientProjectController.projectStatus')
Route.get('/pending_project_count_tech', 'ClientProjectController.pending_project_count_tech')
Route.get('/get_all_project_tech', 'ClientProjectController.get_all_project_tech')
Route.get('/all_project_timeline_tech', 'ClientProjectController.all_project_timeline_tech')
Route.get('/running_project_count_tech', 'ClientProjectController.running_project_count_tech')
Route.get('/get_single_project', 'ClientProjectController.get_single_project')


Route.post('/update_project_tech', 'ClientProjectController.update_project_tech')
Route.post('/project_delete_tech', 'ClientProjectController.project_delete_tech')

//Task
Route.get('/all_task_tech', 'ClientTaskController.all_task_tech')
Route.post('/taskStatus', 'ClientTaskController.taskStatus')
Route.get('/pending_task_count_tech', 'ClientTaskController.pending_task_count_tech')

Route.post('/task_delete_tech', 'ClientTaskController.task_delete_tech')

//Question
Route.get('/all_project_ques_tech', 'ClientQuestionController.all_project_ques_tech')
Route.get('/single_question_tech', 'ClientQuestionController.single_question_tech')
Route.post('/edit_question_tech', 'ClientQuestionController.edit_question_tech')
Route.post('/delete_ques_tech', 'ClientQuestionController.delete_ques_tech')
Route.post('/questionAdd', 'ClientQuestionController.questionAdd')

//Answer
Route.get('/answerShow', 'ClientQuestionController.answerShow')
Route.post('/answerAdd', 'ClientQuestionController.answerAdd')
Route.post('/edit_ans_tech', 'ClientQuestionController.edit_ans_tech')
Route.post('/delete_answer_tech', 'ClientQuestionController.delete_answer_tech')

//Reply
Route.post('/replyAdd', 'ClientQuestionController.replyAdd')
Route.get('/allReplyShow', 'ClientQuestionController.allReplyShow')
Route.post('/edit_reply_tech', 'ClientQuestionController.edit_reply_tech')
Route.post('/delete_reply_tech', 'ClientQuestionController.delete_reply_tech')

//Request
Route.get('/all_request_type_tech', 'ClientTaskController.all_request_type_tech')
Route.post('/timeExtend_req_tech', 'ClientTaskController.timeExtend_req_tech')
Route.get('/all_req_tech', 'ClientTaskController.all_req_tech')
Route.post('/request_delete_tech', 'ClientTaskController.request_delete_tech')

// sales
Route.post('sales/payroll', 'PaymentTransactionController.salesTransactionById')

// uploads
Route.post('/client/project/upload/attachment', 'ClientController.projectSubmitAttachmentUpload')