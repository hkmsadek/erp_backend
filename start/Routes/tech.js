const Route = use('Route')

Route.post('/tech/login', 'TechRequestController.login')

Route.group(() => {
    Route.get('/tech/initdata', 'TechRequestController.initdata')
    //Project
    Route.get('/tech/all_project_tech', 'TechProjectController.all_project_tech')
    Route.get('/tech/all_project_doc_tech', 'TechProjectController.all_project_doc_tech')
    Route.post('/tech/projectStatus', 'TechProjectController.projectStatus')
    Route.get('/tech/pending_project_count_tech', 'TechProjectController.pending_project_count_tech')
    Route.get('/tech/get_all_project_tech', 'TechProjectController.get_all_project_tech')
    Route.get('/tech/all_project_timeline_tech', 'TechProjectController.all_project_timeline_tech')
    Route.get('/tech/running_project_count_tech', 'TechProjectController.running_project_count_tech')
    Route.get('/tech/get_single_project', 'TechProjectController.get_single_project')

    Route.post('/tech/update_project_tech', 'TechProjectController.update_project_tech')
    Route.post('/tech/project_delete_tech', 'TechProjectController.project_delete_tech')
    Route.get('/tech/get_dashboad_project', 'TechProjectController.get_dashboad_project')

    //Task
    Route.get('/tech/all_task_tech', 'TechTaskController.all_task_tech')
    Route.post('/tech/taskStatus', 'TechTaskController.taskStatus')
    Route.get('/tech/pending_task_count_tech','TechTaskController.pending_task_count_tech')

    Route.post('/tech/task_delete_tech','TechTaskController.task_delete_tech')

    //Question
    Route.get('/tech/all_project_ques_tech', 'TechQuestionController.all_project_ques_tech')
    Route.get('/tech/single_question_tech', 'TechQuestionController.single_question_tech')
    Route.post('/tech/edit_question_tech', 'TechQuestionController.edit_question_tech')
    Route.post('/tech/delete_ques_tech', 'TechQuestionController.delete_ques_tech')
    Route.post('/tech/questionAdd', 'TechQuestionController.questionAdd')

    //Answer
    Route.get('/tech/answerShow', 'TechQuestionController.answerShow')
    Route.post('/tech/answerAdd', 'TechQuestionController.answerAdd')
    Route.post('/tech/edit_ans_tech', 'TechQuestionController.edit_ans_tech')
    Route.post('/tech/delete_answer_tech', 'TechQuestionController.delete_answer_tech')

    //Reply
    Route.post('/tech/replyAdd', 'TechQuestionController.replyAdd') 
    Route.get('/tech/allReplyShow', 'TechQuestionController.allReplyShow') 
    Route.post('/tech/edit_reply_tech', 'TechQuestionController.edit_reply_tech') 
    Route.post('/tech/delete_reply_tech', 'TechQuestionController.delete_reply_tech')

    //Request
    Route.get('/tech/all_request_type_tech', 'TechRequestController.all_request_type_tech')
    Route.post('/tech/timeExtend_req_tech', 'TechRequestController.timeExtend_req_tech')
    Route.get('/tech/all_req_tech', 'TechRequestController.all_req_tech')
    Route.post('/tech/request_delete_tech', 'TechRequestController.request_delete_tech')


    // Balance 
    Route.get('/tech/getAgentBalance', 'TechRequestController.getAgentBalance')
    Route.get('/tech/allagentbalance', 'TechRequestController.allagentbalance')


    // Booking || Appointments
    Route.get('/tech/appintments', 'TechVideoRequestController.appintments')
    Route.post('/tech/appintments/editStatus', 'TechVideoRequestController.editAppointmentStatus')
    Route.post('/tech/appintments/add', 'TechVideoRequestController.addAppointment')
    //
    Route.get('/tech/getClientList', 'TechVideoRequestController.getClientList')




    //  *************************** Chat *********************************
    // get chat list ( tech )
    Route.get('/tech/messenger/tech/conversation/get/list', 'TechChatController.getConversationList')
    // last person chat chat
    Route.get('/tech/messenger/tech/conversation/get/chatWithLastPerson/:id', 'TechChatController.getChatWithLastPerson')
    // get chat history by id
    Route.get('/tech/messenger/converstaion/:id', 'TechChatController.getChatHistory')
    // get chat history by username
    Route.get('/tech/messenger/chat/converstaion/:id', 'TechChatController.getChatHistoryByUserId')
    // get previous message of same chat
    Route.get('/tech/messenger/conversation/previous-chat/:conId/:id', 'TechChatController.getMoreMessages')
    // insert chat
    Route.post('/tech/messenger/chat/add', 'TechChatController.insertChat')

    // **************************************************************** ws ******************************************************************************
    Route.post('/tech/messenger/ws/get-last-chat', 'TechChatController.getLastChatMessageToWS')
    // **************************************************************** ws ******************************************************************************

}).middleware(['Tech']);






