const Route = use('Route')
// Route.get('/app/messenger/conversation/get/list', 'AdminChatController.getConversationList')

// // get chat history by id
// Route.get('/app/messenger/converstaion/:id', 'AdminChatController.getChatHistory')



// // insert chat
// Route.post('/app/messenger/chat/add', 'AdminChatController.insertChat')

Route.get('/sadmin/getAllAgent', 'AgentController.getAllAgent')

Route.post('/sadmin/storeAgent', 'AgentController.storeAgent')

Route.post('/sadmin/updateAgent', 'AgentController.updateAgent')

Route.post('/sadmin/deleteAgent', 'AgentController.deleteAgent')

Route.get('/sadmin/getALlUserType', 'AgentController.getALlUserType')

Route.get('/sadmin/getAgentOverviewData', 'AgentController.getAgentOverviewData')

// user
Route.get('/sadmin/getAgentById/:id', 'AgentController.getAgentById')


Route.get('/sadmin/salesRepresentative', 'AgentController.salesRepresentative')
// get over view data


// project
Route.post('/sadmin/assignAgentToProject', 'SprjectController.assignAgentToProject')
Route.post('/sadmin/deleteProject', 'SprjectController.deleteProject')
Route.get('/sadmin/getAllProject', 'SprjectController.getAllProject')
Route.get('/sadmin/getPaymentTransaction', 'SprjectController.getPaymentTransaction')


// task
Route.get('/sadmin/progressProjectTask/:id', 'SprjectController.progressProjectTask')


// Balance 
Route.get('/sadmin/getAgentBalance', 'AdminBalanceController.getAgentBalance')
Route.get('/sadmin/allagentbalance', 'AdminBalanceController.allagentbalance')

// campain
Route.get('/sadmin/getALlCampaignSummary', 'ScampainController.getALlCampaignSummary')
Route.get('/sadmin/getALlCampaign', 'ScampainController.getALlCampaign')
Route.get('/sadmin/getAllCampaignSubscriber', 'ScampainController.getAllCampaignSubscriber')
Route.get('/sadmin/templogin', 'ScampainController.templogin')

Route.post('/sadmin/addnewCampaign', 'ScampainController.addnewCampaign')
Route.post('/sadmin/updateCampaign', 'ScampainController.updateCampaign')
Route.post('/sadmin/deleteCampaign', 'ScampainController.deleteCampaign')
Route.post('/sadmin/deleteSubcriber', 'ScampainController.deleteSubcriber')



// user detail
Route.get('/getAuthUser', 'AgentController.initdata')
Route.post('/sadmin/login', 'AgentController.login')
Route.get('/sadmin/logout', 'AgentController.logout')



// videos category


Route.get('/sadmin/getcategorylist', 'TutorialController.getcategorylist')
Route.get('/sadmin/getALlVideoCategory', 'TutorialController.getALlVideoCategory')
Route.post('/sadmin/addnewVideoCategory', 'TutorialController.addnewVideoCategory')
Route.post('/sadmin/updateVideoCategory', 'TutorialController.updateVideoCategory')
Route.post('/sadmin/deleteVideoCategory', 'TutorialController.deleteVideoCategory')

// video list


Route.get('/sadmin/getALlVideo', 'TutorialController.getALlVideo')
Route.post('/sadmin/addnewVideo', 'TutorialController.addnewVideo')
Route.post('/sadmin/updateVideo', 'TutorialController.updateVideo')
Route.post('/sadmin/deleteVideo', 'TutorialController.deleteVideo')
Route.post('/sadmin/upload-review-file', 'TutorialController.upload')


// perona




Route.get('/sadmin/all_project_tech', 'ProjectController.all_project_tech')
Route.get('/sadmin/all_project_doc_tech', 'ProjectController.all_project_doc_tech')
Route.post('/sadminprojectStatus', 'ProjectController.projectStatus')
Route.get('/sadmin/pending_project_count_tech', 'ProjectController.pending_project_count_tech')
Route.get('/sadmin/get_all_project_tech', 'ProjectController.get_all_project_tech')
Route.get('/sadmin/all_project_timeline_tech', 'ProjectController.all_project_timeline_tech')
Route.get('/sadmin/running_project_count_tech', 'ProjectController.running_project_count_tech')
Route.get('/sadmin/get_single_project', 'ProjectController.get_single_project')

Route.post('/sadmin/update_project_tech', 'ProjectController.update_project_tech')
Route.post('/sadmin/project_delete_tech', 'ProjectController.project_delete_tech')
Route.get('/sadmin/get_dashboad_project', 'ProjectController.get_dashboad_project')

//Task
Route.get('/sadmin/all_task_tech', 'TaskController.all_task_tech')
Route.post('/sadmin/taskStatus', 'TaskController.taskStatus')
Route.get('/sadmin/pending_task_count_tech', 'TaskController.pending_task_count_tech')

Route.post('/sadmin/task_delete_tech', 'TaskController.task_delete_tech')

//Question
Route.get('/sadmin/all_project_ques_tech', 'QuestionController.all_project_ques_tech')
Route.get('/sadmin/single_question_tech', 'QuestionController.single_question_tech')
Route.post('/sadmin/edit_question_tech', 'QuestionController.edit_question_tech')
Route.post('/sadmin/delete_ques_tech', 'QuestionController.delete_ques_tech')
Route.post('/sadmin/questionAdd', 'QuestionController.questionAdd')

//Answer
Route.get('/sadmin/answerShow', 'QuestionController.answerShow')
Route.post('/sadmin/answerAdd', 'QuestionController.answerAdd')
Route.post('/sadmin/edit_ans_tech', 'QuestionController.edit_ans_tech')
Route.post('/sadmin/delete_answer_tech', 'QuestionController.delete_answer_tech')

//Reply
Route.post('/sadmin/replyAdd', 'QuestionController.replyAdd')
Route.get('/sadmin/allReplyShow', 'QuestionController.allReplyShow')
Route.post('/sadmin/edit_reply_tech', 'QuestionController.edit_reply_tech')
Route.post('/sadmin/delete_reply_tech', 'QuestionController.delete_reply_tech')

//Request
Route.get('/sadmin/all_request_type_tech', 'RequestController.all_request_type_tech')
Route.post('/sadmin/timeExtend_req_tech', 'RequestController.timeExtend_req_tech')
Route.get('/sadmin/all_req_tech', 'RequestController.all_req_tech')
Route.post('/sadmin/request_delete_tech', 'RequestController.request_delete_tech')



// messenger api


// get chat list ( agent )
Route.get('/sadmin/app/messenger/conversation/get/list', 'AdminChatController.getClientConversationList')

// last person chat chat
Route.get('/sadmin/app/messenger/conversation/get/chatWithLastPerson/:id', 'AdminChatController.getClientChatWithLastPerson')


// get chat history by id
Route.get('/sadmin/app/messenger/converstaion/:id', 'AdminChatController.getChatHistory')

// get chat history by username
Route.get('/sadmin/app/messenger/chat/converstaion/:id', 'AdminChatController.getChatHistoryByUserId')

// get previous message of same chat
Route.get('/sadmin/app/messenger/conversation/previous-chat/:conId/:id', 'AdminChatController.getMoreMessages')


// insert chat
Route.post('/sadmin/app/messenger/chat/add', 'AdminChatController.insertChat')