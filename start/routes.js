'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
require('./Routes/supperadmin');
require('./Routes/SalesRep');
require('./Routes/tech');
require('./Routes/client');
const Route = use('Route');

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.get('/login1', 'UserController.user')

Route.post('/post', 'UserController.getUser')
