'use strict'

const VideoRequest = use('App/Models/VideoRequest')
const ProjectEmployee = use('App/Models/ProjectEmployee')
const Database = use('Database')

class TechVideoRequestController {
  
        // Apointments
        async appintments({ request, response,auth}){
          let user;
          try {
              user = await auth.getUser()
          } catch (error) {
              return response.status(401).json({
                  message: 'You are not authorized!'
              })
          }
          let page = request.input('page') ? request.input('page') : 1;
          let total = request.input('total') ? request.input('total') : 5;
          let type = request.input('type') ? request.input('type') : 'receiver';
          let str = request.input('str');
          let data =  VideoRequest.query().with('project').with('sender');
          if(type== 'receiver') data.where('receiver_id',user.id);
          if(type== 'sender') data.where('user_id',user.id);
          if(type== 'all') {
              data.where((builder)=>{
                  builder.where('user_id',user.id);
                  builder.orWhere('receiver_id',user.id);
              });
          }
          if(str) {
  
              data.where((builder)=>{
                  builder.whereHas('client' ,(builder2)=>{
                      builder2.where('firstname', 'like', '%'+str+'%');
                  });
                  builder.orWhere('name', 'like', '%'+str+'%');
              });
          } 
          let finalData = await  data.orderBy('date','desc').paginate(page,total);
          // return data
          return response.status(200).json({
            'success': true,
            'data': finalData,
          });
      }
      async editAppointmentStatus({ request, response,auth}){
          let data = request.all();
          let user;
          try {
              user = await auth.getUser()
          } catch (error) {
              return response.status(401).json({
                  'success': false,
                  message: 'You are not authorized!'
              })
          }
          
          let finalData = await  VideoRequest.query().where('id',data.id).update(data);
          // return data
          return response.status(200).json({
            'success': true,
            'data': finalData,
          });
      }
      async addAppointment({ request, response,auth}){
          let data = request.all();
          let user = await auth.getUser();
          data.user_id = user.id;
          
          let rawData = await  VideoRequest.create(data);
          let finalData = await VideoRequest.query().with('project').with('sender').where('id',rawData.id).first();
          // return data
          return response.status(200).json({
            'success': true,
            'data': finalData,
          });
      }
  
  
      // Query  Methods
      async getClientList({ request, response,auth}){
          let user;
          try {
              user = await auth.getUser()
              
          } catch (error) {
              return response.status(401).json({
                  message: 'You are not authorized!'
              })
          }
          let str = request.input('str');
          let projects = await  ProjectEmployee.query().where('user_id',user.id).pluck('project_id');
          let data = Database
          .select('projects.*','users.firstname','users.lastname')
          .from('projects')
          .innerJoin('users', 'projects.user_id', 'users.id')
          data.whereIn('projects.id',projects);
          
          // let data =  VideoRequest.query().with('project').with('sender').where('receiver_id',user.id);
          if(str){
              data.where((builder) =>{
                builder.where('users.firstName', 'like', '%'+str+'%');
                builder.orWhere('users.lastName', 'like', '%'+str+'%');
                builder.orWhere('projects.name', 'like', '%'+str+'%');
                builder.orWhere('projects.user_id',str);
              });
          }  
          let finalData = await  data.limit(5);
          // return data
          return response.status(200).json({
            'success': true,
            'data': finalData,
          });
      }
}

module.exports = TechVideoRequestController
