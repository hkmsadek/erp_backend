'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tutorial = use('App/Models/Tutorial')
const UserType = use('App/Models/UserType')
const User = use('App/Models/User')
const Helpers = use('Helpers')
const TutorialCategory = use('App/Models/TutorialCategory')

class TutorialController {


  // videos category 
  
   async getcategorylist ({ request, response, view, auth  }) {
     return await TutorialCategory.all()
   }
   async getALlVideoCategory ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
            let page = request.input('page') ? request.input('page') : 1
            let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
            let str = request.input('str') ? request.input('str') : ''
            let order = request.input('order') ? request.input('order') : 'desc'
            let colName = request.input('colName') ? request.input('colName') : 'id'
           
           let q =  TutorialCategory.query()
           if(str){
             q.where('name', 'LIKE', '%' + str + '%')
           }

          return await q.orderBy(colName, order).paginate(page,pageSize)

    }
   async addnewVideoCategory ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
            if(!data.name){
              return response.status(401).json({
                message: "Field name cannot be null or empty!!"
              })
            }
     
           let ob ={
             name:data.name
           }
           return  await TutorialCategory.create(ob)
    

    }
   async deleteVideoCategory ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
            let check = await Tutorial.query().where('tutorial_category_id', data.id).first()
            if(check){
               return response.status(401).json({
                 'message': 'you delete this , all video under this category will deteted!!'
               })
            }
     
           let ob ={
             name:data.name
           }
           return await TutorialCategory.query().where('id', data.id).delete()
    
    }
   async updateVideoCategory ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
       
     
           let ob ={
             name:data.name
           }
            await TutorialCategory.query().where('id', data.id).update(ob)
         return await TutorialCategory.query().where('id', data.id).first()
    
    }

    // video 
    
    
     async updateVideo({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
       
     
           let ob ={
             title:data.title,
             url:data.url,
             description:data.description,
             tutorial_category_id:data.tutorial_category_id
           }
            await Tutorial.query().where('id', data.id).update(ob)
          return await Tutorial.query().where('id', data.id).with('category').first()
    
    }

       async deleteVideo ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
  
           return await Tutorial.query().where('id', data.id).delete()
    
    }


       async addnewVideo ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
     
            let ob ={
             title:data.title,
             url:data.url,
             description:data.description,
             tutorial_category_id:data.tutorial_category_id
           }
           let d =  await Tutorial.create(ob)
           return Tutorial.query().where('id', d.id).with('category').first()
    

    }


     async upload ({ request, response, view, auth  }) {

        const profilePic = request.file('file', {
          // types: ['png', 'jpg', 'jpeg'],
          // size: '2mb'
        })

        const name = `${new Date().getTime()}` + '.' + profilePic.subtype
        // UPLOAD THE IMAGE TO UPLOAD FOLDER
        await profilePic.move(Helpers.publicPath('uploads'), {
          name: name
        })
        if (!profilePic.moved()) {
          return profilePic.error()
        }

        return response.status(200).json({
          msg: 'Image has been uploaded successfully!',
          file: `/uploads/${name}`
        })
      
     }
     async getALlVideo ({ request, response, view, auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }

        

            let data = request.all()
            let page = request.input('page') ? request.input('page') : 1
            let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
            let str = request.input('str') ? request.input('str') : ''
            let catId = request.input('catId') ? request.input('catId') : ''
            let order = request.input('order') ? request.input('order') : 'desc'
            let colName = request.input('colName') ? request.input('colName') : 'id'
           
           let q = Tutorial.query().with('category')
           if(str){
             q.where('title', 'LIKE', '%' + str + '%')
            .orWhereHas('category', (builder) => {
              builder.where('name', 'LIKE', '%' + str + '%')

            })
           }
           if(catId){
             q.where('tutorial_category_id', catId)
           }
           if (colName=='name'){
             q.orWhereHas('category', (builder) => {
               builder.orderBy(colName, order)

             })
           }
           else{
             q.orderBy(colName, order)
           }

          return await q.paginate(page,pageSize)

    }





 
  
}

module.exports = TutorialController
