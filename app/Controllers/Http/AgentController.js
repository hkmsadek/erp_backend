'use strict'
const User = use('App/Models/User')
const UserType = use('App/Models/UserType')
const Project = use('App/Models/Project')
const Task = use('App/Models/Task')
const UserBalance = use('App/Models/UserBalance')
const Database = use('Database')

class AgentController {

  async logout({request, response, params, auth}){
      await auth.logout()
      return 'logged out'
  }
    async login({ request, response, auth }) {
        const data = request.all()
        try {
            let user = await auth.query().attempt(data.email, data.password)
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            if(usertype.name!='Admin'){
                   await auth.logout()
                      return response.status(401).json({
                        'message': 'Your are not authenticate user!!'
                      })
            }
            
            user.userType = usertype
            

            return response.status(200).json(user)
        } catch (e) {
            // console.log(e.message)
            return response.status(401).json(
                {
                    'message': 'Invalid email or password. Please try again.'
                }
            )
        }
    }

    async initdata({ request, response, auth }) {
        try {
            let user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            return user
        } catch (error) {
            return false
        }
    }

        async storeUser ({ request, response }) {  
    
        let reqData = request.all()
        const rules = {
            email: 'required|email|unique:users',
            password: 'required',
            username: 'required',
        }
        const validation = await validate(request.all(), rules)
    
        if (validation.fails()) {
            return response.status(401).json(validation.messages())
        }
        return await User.create(reqData) 
      }



    async getAllAgent ({ request, response, view ,auth }) {
          let user
          try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
              await auth.logout()
              return response.status(401).json({
                'message': 'Your are not authenticate user!!'
              })
            }
          } catch (error) {
            await auth.logout()
            return response.status(403).json({
              message: "You are not authenticate user"
            })
          }


        let page = request.input('page') ? request.input('page') : 1
        let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
        let order = request.input('order') ? request.input('order') : 'desc'
         let colName = request.input('colName') ? request.input('colName') : 'id'
        let str = request.input('str') ? request.input('str') : ''

        let data = request.all()
      

        let q =   User.query().whereHas('usertype', (builder) => {
           builder.where('name', 'Admin')
          
         })
         q.orWhereHas('usertype', (builder) => {

           builder.orWhere('name', 'Sales Representative')
         })
         q.orWhereHas('usertype', (builder) => {

           builder.orWhere('name', 'Technician')
         })
         if(str){
          q.where('firstname', 'LIKE', '%' + str + '%')
          q.orWhere('lastname', 'LIKE', '%' + str + '%')
         }
         if (order){
           q.orderBy(colName, order)
         }
         
         return await q.paginate(page, pageSize)
    }
    
    async getAgentById ({ request, response, view ,params,auth }) {
        let user
        try {
          user = await auth.getUser()
          let usertype = await UserType.query().where('id', user.user_type_id).first()
          user.userType = usertype
          if (usertype.name != 'Admin') {
            await auth.logout()
            return response.status(401).json({
              'message': 'Your are not authenticate user!!'
            })
          }
        } catch (error) {
          await auth.logout()
          return response.status(403).json({
            message: "You are not authenticate user"
          })
        }

         let agent = await User.query().where('id', params.id).first()
         return agent;
    }

    async storeAgent({request, response,view,auth }){
        let user
        try {
          user = await auth.getUser()
          let usertype = await UserType.query().where('id', user.user_type_id).first()
          user.userType = usertype
          if (usertype.name != 'Admin') {
            await auth.logout()
            return response.status(401).json({
              'message': 'Your are not authenticate user!!'
            })
          }
        } catch (error) {
          await auth.logout()
          return response.status(403).json({
            message: "You are not authenticate user"
          })
        }

         let data = request.all()
         return await User.create(data)




    }
    async updateAgent({request, response,view ,auth }){
        let user
        try {
          user = await auth.getUser()
          let usertype = await UserType.query().where('id', user.user_type_id).first()
          user.userType = usertype
          if (usertype.name != 'Admin') {
            await auth.logout()
            return response.status(401).json({
              'message': 'Your are not authenticate user!!'
            })
          }
        } catch (error) {
          await auth.logout()
          return response.status(403).json({
            message: "You are not authenticate user"
          })
        }

         let data = request.all()
           await User.query().where('id', data.id).update(data)
           return User.query().where('id', data.id).first()
         
     //     return await User.create(data)




    }
    async getAgentOverviewData({request, response,view,auth  }){
          let user
          try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
              await auth.logout()
              return response.status(401).json({
                'message': 'Your are not authenticate user!!'
              })
            }
          } catch (error) {
            await auth.logout()
            return response.status(403).json({
              message: "You are not authenticate user"
            })
          }
          let data = request.all()

       if (data.agentId) {
            let totalProject = await Project.query()

              .whereHas('details', (builder) => {
                builder.where('agent_id', data.agentId)
              }).count('* as total')



            let pending = await Project.query().where('status', 'Pending')

              .whereHas('details', (builder) => {
                builder.where('agent_id', data.agentId)
              }).count('* as total')

            let processing = await Project.query().where('status', 'Processing')

              .whereHas('details', (builder) => {
                builder.where('agent_id', data.agentId)
              }).count('* as total')

            let completed = await Project.query().where('status', 'Completed')

              .whereHas('details', (builder) => {
                builder.where('agent_id', data.agentId)
              }).count('* as total')
            let totalTechnician = await Task.query().distinct('user_id').where('agent_id', data.agentId)

              .where((builder) => {
                builder.where('status', 'Processing').orWhere('status', 'Pending')

              }).fetch()

            let d = Project.query().with('details').with('user')
            if (data.agentId) {
              d.whereHas('details', (builder) => {
                builder.where('agent_id', data.agentId)
              })
            }

            let projectList = await d.limit(5).fetch()
            let totalbalance = await Database.from('user_balances').sum('amount as totalbalance').where('user_id', data.agentId).first()
            let balancestatement = await UserBalance.query().where('user_id', data.agentId).with('user').orderBy('id', 'desc').limit(5)
              .fetch()
      



          
          return {
            balancestatement,
            totalProject,
            projectList,
            totalTechnician,
            pending,
            processing,
            completed,
            totalbalance,
           }
 }


    }
    async getALlUserType({request, response,view ,auth }){
      let user
      try {
        user = await auth.getUser()
        let usertype = await UserType.query().where('id', user.user_type_id).first()
        user.userType = usertype
        if (usertype.name != 'Admin') {
          await auth.logout()
          return response.status(401).json({
            'message': 'Your are not authenticate user!!'
          })
        }
      } catch (error) {
        await auth.logout()
        return response.status(403).json({
          message: "You are not authenticate user"
        })
      }

         return await UserType.all()




    }
    async salesRepresentative({request, response,view ,auth }){
       let user
       try {
         user = await auth.getUser()
         let usertype = await UserType.query().where('id', user.user_type_id).first()
         user.userType = usertype
         if (usertype.name != 'Admin') {
           await auth.logout()
           return response.status(401).json({
             'message': 'Your are not authenticate user!!'
           })
         }
       } catch (error) {
         await auth.logout()
         return response.status(403).json({
           message: "You are not authenticate user"
         })
       }

         return await User.query().whereHas('usertype', (builder) => {
           builder.where('name', 'Sales Representative')
         }).fetch()




    }



}

module.exports = AgentController
