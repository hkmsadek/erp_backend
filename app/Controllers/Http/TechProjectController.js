'use strict'

const Project = use('App/Models/Project')
const ProjectEmployee = use('App/Models/ProjectEmployee')
const Attachment = use('App/Models/Attachment')
const ProjectTimeline = use('App/Models/ProjectTimeline')
const Database = use('Database')
class TechProjectController {

  async all_project_tech({request,response,auth}){
    let user = {}
    try {
        user = await auth.getUser()
    } catch (error) {
        return response.status(403).json({
            message: 'You are not authorized!'
        })
    }
    let page = request.input('page');
    let pageSize = request.input('pageSize');
    let status = request.input('status');
    let searchKey = request.input('searchFilter');
    let query = Project.query()

    if(status=='Processing'){
      query.where('status', status)
    }
    else if (status == 'Pending') {
      query.where('status', status)
    }

    let res 
    if(searchKey){
      res = await query
      .with('user')
      .withCount('task as total_task')
      .withCount('task as completed_task', (builder) => {
        builder.where('status','Completed')
      })
      .where((builder) => {
        builder.whereIn('id', (builder) => {
            builder.select('project_id')
                .from('project_employees')
                .where('user_id', user.id)
        })
      })
      .whereIn('id', (builder) => {
        builder.select('id')
            .from('projects')
            .where(Database.raw(`name LIKE "%${searchKey}%"`))
            .orWhere(Database.raw(`description LIKE "%${searchKey}%"`))
            .orWhere(Database.raw(`tech_price LIKE "%${searchKey}%"`))
        })
      .where('isDeleted',0)
      .orderBy('id','desc')
      .paginate(page,pageSize)
    } 
    else{
      res = await query
      .with('user')
      .withCount('task as total_task')
      .withCount('task as completed_task', (builder) => {
        builder.where('status','Completed')
      })
      .where((builder) => {
        builder.whereIn('id', (builder) => {
            builder.select('project_id')
                .from('project_employees')
                .where('user_id', user.id)
        })
      })
      .where('isDeleted',0)
      .orderBy('id','desc')
      .paginate(page,pageSize)
    }

     return res; 
  }

  async all_project_doc_tech({request,response}){
        return await Attachment.query()
                    .with('user')
                    .with('project')
                    .where('project_id',request.input('project_id'))
                    .orderBy('id','desc')
                    .fetch()

  }

  async projectStatus({request,response}){
    let data = request.all()
    let projcetId = request.input('projcetId');
    if( data.status=='Processing'){
      const now = new Date();
      await Project.query().where('id', projcetId).update({
          status: data.status,
          startDate: now,
      })
      return Project.query().where('id', projcetId).first()
    }
    else{
      return await Project.query().where('id', projcetId).update({
        status: data.status
      })
    }
  }

  async pending_project_count_tech({request,response,auth}){
    let user = {}
    try {
        user = await auth.getUser()
    } catch (error) {
        return response.status(403).json({
            message: 'You are not authorized!'
        })
    }
    return await Project.query()
                .where((builder) => {
                  builder.whereIn('id', (builder) => {
                      builder.select('project_id')
                          .from('project_employees')
                          .where('user_id',user.id)
                  })
                })
                .where('isDeleted',0)
                .where('status','Pending')
                .getCount()
  }

  async get_all_project_tech({request,response,auth}){
    let user = {}
    try {
        user = await auth.getUser()
    } catch (error) {
        return response.status(403).json({
            message: 'You are not authorized!'
        })
    }
    return await Project.query()
                        .where((builder) => {
                          builder.whereIn('id', (builder) => {
                              builder.select('project_id')
                                  .from('project_employees')
                                  .where('user_id',user.id)
                          })
                        })
                        .where('status','!=','pending')
                        .where('isDeleted',0)
                        .orderBy('id','desc')
                        .fetch()

  }


  async all_project_timeline_tech({request,response}){
          return await ProjectTimeline.query()
                .with('task')
                .with('project')
                .where('project_id',request.input('project_id'))
                .orderBy('id','desc')
                .fetch()

  }

  async running_project_count_tech({request,response,auth}){
          let user = {}
          try {
              user = await auth.getUser()
          } catch (error) {
              return response.status(403).json({
                  message: 'You are not authorized!'
              })
          }
          return await Project.query()
                .where((builder) => {
                  builder.whereIn('id', (builder) => {
                      builder.select('project_id')
                          .from('project_employees')
                          .where('user_id',user.id)
                  })
                })
                .where('isDeleted',0)
                .where('status','Processing')
                .getCount()
    }

    async get_single_project({request,response,params}){
        return await Project.query()
                    .with('user')
                    .withCount('question as total_question')
                    .where('id',request.input('project_id'))
                    .orderBy('id','desc')
                    .first() 

    }

    async update_project_tech({request,response,params}){
      let data = request.all()
        return await Project.query()
                    .where('id',data.id)
                    .update({
                      tech_price:data.tech_price,
                      hour:data.hour,
                    })
                    

    }

    async project_delete_tech({request,response,params}){
      let data = request.all()
        return await Project.query()
                    .where('id',data.id)
                    .update({
                      isDeleted:1
                  })

    }

    async get_dashboad_project({request,response,params,auth}){
      let user = {}
      try {
          user = await auth.getUser()
      } catch (error) {
          return response.status(403).json({
              message: 'You are not authorized!'
          })
      }
      return await Project.query()
                          .with('user')
                          .withCount('task as total_task')
                          .withCount('task as completed_task', (builder) => {
                            builder.where('status','Completed')
                          })
                          .where((builder) => {
                            builder.whereIn('id', (builder) => {
                                builder.select('project_id')
                                    .from('project_employees')
                                    .where('user_id',user.id)
                            })
                          })
                          .where('isDeleted',0)
                          .limit(5)
                          .orderBy('id','desc')
                          .fetch()

    }

    

    

  


}

module.exports = TechProjectController
