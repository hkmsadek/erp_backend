'use strict'
const Question = use('App/Models/Question')
const Answer = use('App/Models/Answer')
const AnswerReply = use('App/Models/AnswerReply')

class TechQuestionController {

  // ---------------- Question ------------------------
  async all_project_ques_tech({request,response}){
    if (request.input('last_id')) {
          return await Question.query()
          .with('user')
          .with('project')
          .with('answer.user')
          .withCount('answer as total_answer')
          .where('id', '<', request.input('last_id'))
          .where('project_id',request.input('project_id'))
          .limit(10)
          .orderBy('id','desc')
          .fetch()
    }
    return await Question.query()
          .with('user')
          .with('project')
          .with('answer.user')
          .withCount('answer as total_answer')
          .where('project_id',request.input('project_id'))
          .limit(10)
          .orderBy('id','desc')
          .fetch()
  }
  async delete_ques_tech ({ request, response }) {
    let reqData = request.all()
    return await Question.query().where('id',reqData.id).delete()
  }

  async single_question_tech({request,response}){
    return await Question.query()
          .with('user')
          .with('project')
          .with('answer', (builder) => {
              builder.orderBy('id', 'desc')
              builder.with('user')
          })
          .withCount('answer as total_answer')
          .where('id',request.input('question_id'))
          .fetch()
  }
  async edit_question_tech ({ request, response }) {
    let reqData = request.all()
    return await Question.query().where('id',reqData.id).update(reqData)
  }
  async questionAdd({request,response}){
    let reqData = request.all()
    let createData= await Question.create(reqData)
    return await Question.query()
                      .with('user')
                      .with('project')
                      .with('answer.user')
                      .withCount('answer as total_answer')
                      .where('project_id',reqData.project_id)
                      .limit(10)
                      .orderBy('id','desc')
                      .fetch()
    }
 

  // ---------------- Answer ------------------------
  async answerShow({request,response}){
    if(request.input('last_id')) {
        return await Answer.query()
        .with('user')
        .withCount('reply as total_reply')
        .where('id', '<', request.input('last_id'))
        .where('question_id',request.input('question_id'))
        .limit(10)
        .orderBy('id','desc')
        .fetch()
    }
    return await Answer.query()
          .with('user')
          // .with('reply.user')
          .withCount('reply as total_reply')
          .where('question_id',request.input('question_id'))
          .limit(10)
          .orderBy('id','desc')
          .fetch()
  }

  async answerAdd({request,response}){
    let reqData = request.all()
    let createData= await Answer.create(reqData)
    let page = request.input('page');
    return await Answer.query()
                      .with('user')
                      .withCount('reply as total_reply')
                      .where('question_id',reqData.question_id)
                      .limit(10)
                      .orderBy('id','desc')
                      .fetch()
    }
    async delete_answer_tech ({ request, response }) {
      let reqData = request.all()
      return await Answer.query().where('id',reqData.id).delete()
    }
    async edit_ans_tech ({ request, response }) {
      let reqData = request.all()
      return await Answer.query().where('id',reqData.id).update({
        content:reqData.content
      })
    }


    // ---------------- Reply ------------------------
    async replyAdd({request,response}){
        let reqData = request.all()
        await AnswerReply.create(reqData)
        return await AnswerReply.query()
                        .with('user')
                        .where('answer_id',reqData.answer_id)
                        .limit(10)
                        .orderBy('id','desc')
                        .fetch()
      }
      async allReplyShow({request,response}){
        if(request.input('last_id')) {
            return await AnswerReply.query()
            .with('user')
            .where('id', '<', request.input('last_id'))
            .where('answer_id',request.input('answer_id'))
            .limit(10)
            .orderBy('id','desc')
            .fetch()
        }
        return await AnswerReply.query()
              .with('user')
              .where('answer_id',request.input('answer_id'))
              .limit(10)
              .orderBy('id','desc')
              .fetch()
      }
      async delete_reply_tech ({ request, response }) {
        let reqData = request.all()
        return await AnswerReply.query().where('id',reqData.id).delete()
      }

      async edit_reply_tech ({ request, response }) {
        let reqData = request.all()
        return await AnswerReply.query().where('id',reqData.id).update({
          content:reqData.content
        })
      }

      

      

      

}

module.exports = TechQuestionController
