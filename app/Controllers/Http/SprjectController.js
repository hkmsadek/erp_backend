'use strict'

// const User = require("../../Models/User")

const User = use('App/Models/User')
const UserType = use('App/Models/UserType')
const Project = use('App/Models/Project')
const ProjectEmployee = use('App/Models/ProjectEmployee')
const PaymentTransaction = use('App/Models/PaymentTransaction')

class SprjectController {

    
    async deleteProject ({ request, response, view,auth  }) {
            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }
        let data = request.all()
        return Project.query().where('id',data.id).delete()


    }
    async assignAgentToProject ({ request, response, view,auth  }) {

            let user
            try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
                })
            }
            } catch (error) {
            await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
            }
        let data = request.all()

        let ob={
            project_id: data.project_id,
            user_id: data.user_id,
        }
        await Project.query().where('id',ob.project_id).update({status:'Processing'})
        return await ProjectEmployee.create(ob)

    }
    async getAllProject ({ request, response, view ,auth }) {
            let user
            try {
              user = await auth.getUser()
              let usertype = await UserType.query().where('id', user.user_type_id).first()
              user.userType = usertype
              if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                  'message': 'Your are not authenticate user!!'
                })
              }
            } catch (error) {
              await auth.logout()
              return response.status(403).json({
                message: "You are not authenticate user"
              })
            }

          let page = request.input('page') ? request.input('page') : 1
          let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
          let str = request.input('str') ? request.input('str') : ''
          let order = request.input('order') ? request.input('order') : 'desc'
          let colName = request.input('colName') ? request.input('colName') : 'id'
          let status = request.input('status') ? request.input('status') : ''
          let data = request.all()
      
          let q = Project.query().where('status', status).with('user.usertype')
          if(str){

               q.where('name', 'LIKE', '%' + str + '%')
          }
          if (order) {

               q.orderBy(colName, order)
          }
          
         let d = await q.paginate(page, pageSize)
         return d

    }

   async getPaymentTransaction({request,response,view,auth}){
       let user
       try {
         user = await auth.getUser()
         let usertype = await UserType.query().where('id', user.user_type_id).first()
         user.userType = usertype
         if (usertype.name != 'Admin') {
           await auth.logout()
           return response.status(401).json({
             'message': 'Your are not authenticate user!!'
           })
         }
       } catch (error) {
         await auth.logout()
         return response.status(403).json({
           message: "You are not authenticate user"
         })
       }


       let data = request.all()
       let page = request.input('page') ? request.input('page') : 1
       let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
       let str = request.input('str') ? request.input('str') : ''
       let order = request.input('order') ? request.input('order') : 'desc'
       let colName = request.input('colName') ? request.input('colName') : 'id'

       let q = PaymentTransaction.query().with('user.usertype').with('project').where('project_id', data.projectId)
       if (str) {

         q.whereHas('user', (builder) => {
           builder.where('firstname', 'LIKE', '%' + str + '%')
         })
         q.orWhereHas('user', (builder) => {
           builder.where('lastname', 'LIKE', '%' + str + '%')
         })
       
       }

       let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
       return alldata
   }


  



}

module.exports = SprjectController
