'use strict'

const RequestType = use('App/Models/RequestType')
const Request= use('App/Models/Request')
const UserBalance = use('App/Models/UserBalance')
const User = use('App/Models/User')
const Database = use('Database')

class TechRequestController {
    // Login
    async login({ request, response, auth }){

        const { email, password } = request.all()
        let user = await User.query().where('email',email).first()
        if(user){
          if (user.user_type_id == 4) {
            return auth.attempt(email, password)
          } 
          else{
            return response.status(401).json(
              [{"message": "You're not an authenticated technician!"}]
            );
          }
        }
        else{
          return response.status(401).json(
            [{"message": "Invalid email or password. Please try again!"}]
          );
        }
    }
    async initdata ({ request, response, auth }) {
        try {
          const user = await auth.getUser()
         // console.log(user)
          return {
            user: user
          }
        } catch (error) {
          console.log(error.message)
          return false
        }
    }
    // Tech Balance 
    async getAgentBalance ({ request, response, view ,auth }) {
        let user = await auth.getUser()
        const alldata = await  UserBalance.query().where('user_id', user.id).getSumDistinct('amount')
        return alldata
    }
    
    async allagentbalance ({ request, response, view,auth  }) {
    let user = await auth.getUser()

    let data = request.all()
    let page = request.input('page') ? request.input('page') : 1
    let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
    let str = request.input('str') ? request.input('str') : ''
    let order = request.input('order') ? request.input('order') : 'desc'
    let colName = request.input('colName') ? request.input('colName') : 'id'

    let  q  =  UserBalance.query().with('project').where('user_id', user.id)
    if(str){

        q.whereHas('project', (builder) => {
        builder.where('name', 'LIKE', '%' + str + '%')
        })
    }
    
    let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
    return alldata


    }

    // All Request Type
    async all_request_type_tech({request,response}){
        return await RequestType.all();
    }

    async timeExtend_req_tech({request,response}){
        let reqData = request.all()
        let res= await Request.create(reqData)
        if(res){
        return Request.query()
                        .with('user')
                        .with('project')
                        .with('request_type')
                        .orderBy('id','desc')
                        .first()
        }
        return;
    }

    async all_req_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page');
        let pageSize  = request.input('pageSize');
        let req_type = request.input('req_type');
        let searchKey = request.input('searchFilter');
        let query = Request.query()
    
        if(req_type=='Sent'){
            query.where('user_id', user.id)
        }
        else{
            query.where('receiver_id', user.id)
        }

        let res 
        if(searchKey){
            res = await query
            .with('user')
            .with('project')
            .with('request_type')
            .whereIn('id', (builder) => {
            builder.select('id')
                .from('requests')
                .orWhere(Database.raw(`description LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`status LIKE "%${searchKey}%"`))
            })
            .where('isDeleted',0)
            .orderBy('id','desc')
            .paginate(page,pageSize)
        }
        else{
            res = await query
            .with('user')
            .with('project')
            .with('request_type')
            .where('isDeleted',0)
            .orderBy('id','desc')
            .paginate(page,pageSize)
        }
        return res;
    }

    async request_delete_tech({request,response,params}){
        let data = request.all()
        return await Request.query()
                    .where('id',data.id)
                    .update({
                        isDeleted:1
                    })

    }

}

module.exports = TechRequestController
