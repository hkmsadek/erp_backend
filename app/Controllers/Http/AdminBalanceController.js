'use strict'

const User = use('App/Models/User')
const Project = use('App/Models/Project')
const UserBalance = use('App/Models/UserBalance')
const UserType = use('App/Models/UserType')
const Database = use('Database')

class AdminBalanceController {
     async allagentbalance ({ request, response, view,auth  }) {
          let user
          try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
              await auth.logout()
              return response.status(401).json({
                'message': 'Your are not authenticate user!!'
              })
            }
          } catch (error) {
            await auth.logout()
            return response.status(403).json({
              message: "You are not authenticate user"
            })
          }


        let data = request.all()
         let page = request.input('page') ? request.input('page') : 1
         let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
         let str = request.input('str') ? request.input('str') : ''
         let order = request.input('order') ? request.input('order') : 'desc'
         let colName = request.input('colName') ? request.input('colName') : 'id'

        let  q  =  UserBalance.query().with('user.usertype').where('user_id', data.agentId)
         if(str){

          q.whereHas('user', (builder) => {
            builder.where('firstname', 'LIKE', '%' + str + '%')
            .orWhere('lastname', 'LIKE', '%' + str + '%')
          })
       }
        
        let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
        return alldata


     }


     async getAgentBalance ({ request, response, view ,auth }) {

          let user
          try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
              await auth.logout()
              return response.status(401).json({
                'message': 'Your are not authenticate user!!'
              })
            }
          } catch (error) {
            await auth.logout()
            return response.status(403).json({
              message: "You are not authenticate user"
            })
          }



        //  let data = request.all()
         let page = request.input('page') ? request.input('page') : 1
         let str = request.input('str') ? request.input('str') : ''
         let price = request.input('price') ? request.input('price') : ''
         let order = request.input('order') ? request.input('order') : 'desc'
         let colName = request.input('colName') ? request.input('colName') : 'id'

        // if(data.type='agent'){
        //      let a =  Database
        //     .table('users')
        //     .select('*')
        //     .rightOuterJoin('user_types', function () {
        //         this
        //         .on('users.user_types_id', 'user_types.id')
        //     })
        //     .rightOuterJoin('user_balances', function () {
        //         this
        //         .on('user_balances.user_id', 'users.id')
        //     }).select('user_types.name as userType')
        //     .sum('user_balances.amount as totalamount')
            
            
        //     .where('user_types.name', 'Sales Representative')
        //     .orWhere('user_types.name', 'Admin')
        //     .orWhere('user_types.name', 'Technician')
        //     return await a.paginate(page, 1)
        // }
        // else{
        //     return await Database
        //         // .table('users')
        //         .select('*')
        //         .from('users')
                
        //         .innerJoin('user_types', function () {
        //         this
        //             .on('users.user_types_id', 'user_types.id')
        //         })
        //         .innerJoin('user_balances', function () {
        //         this
        //             .on('user_balances.user_id', 'users.id')
        //         })
        //         .sum('user_balances.amount as totalamount')
        //         .where('user_types.name', 'Client')
        //         .orWhere('user_types.name', 'Prospective Client')
                 
        // }

        // price = 2


        let data = Database
          .select('users.*', 'user_types.name as userType')
          
          .from('users')
          .innerJoin('user_types', 'user_types.id', 'users.user_type_id')
          .innerJoin('user_balances', 'user_balances.user_id', 'users.id')
          
           .sum('user_balances.amount as totalamount')
        //   .orderBy('user_balances.amount', 'desc')

           if (str) {
                 data.where('users.firstname', 'LIKE', '%' + str + '%')
                 data.orWhere('users.lastname', 'LIKE', '%' + str + '%')
           }
            

        
        const alldata = await data.orderBy(colName, order).groupBy('user_id').paginate(page, 10);
        return alldata





     }

    
}

module.exports = AdminBalanceController
