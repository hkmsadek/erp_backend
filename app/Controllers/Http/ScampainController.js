'use strict'
const User = use('App/Models/User')
const Project = use('App/Models/Project')
const UserType = use('App/Models/UserType')
const Campaign = use('App/Models/Campaign')
const CampaignSubscriber = use('App/Models/CampaignSubscriber')
const Database = use('Database')
class ScampainController {

        async templogin ({ request, response, view,auth }) {
           let data={
                email: 'peru@gmail.com',
                password:'123456'
            }

             let user = await auth.query().attempt(data.email, data.password)
             return "login"
        }
        async getALlCampaign ({ request, response, view,auth }) {
            let user
            try {
              user = await auth.getUser()
              let usertype = await UserType.query().where('id', user.user_type_id).first()
              user.userType = usertype
              if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                  'message': 'Your are not authenticate user!!'
                })
              }
            } catch (error) {
              await auth.logout()
              return response.status(403).json({
                message: "You are not authenticate user"
              })
            }

            let page = request.input('page') ? request.input('page') : 1
            let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
            let order = request.input('order') ? request.input('order') : 'desc'
            let colName = request.input('colName') ? request.input('colName') : 'id'
            let str = request.input('str') ? request.input('str') : ''

            let q =  Campaign.query().orderBy(colName, order)

                if(str){
                    q.where('campaignName', 'LIKE', '%' + str + '%')
                    .orWhere('campaignBudget', 'LIKE', '%' + str + '%')
                    .orWhere('campaignDuration', 'LIKE', '%' + str + '%')
                  
                }
            
            let campaign = await q.paginate(page, pageSize)
            return campaign

        }
        async getALlCampaignSummary ({ request, response, view,auth }) {
            let user 
             try {
                user = await auth.getUser()
                 let usertype = await UserType.query().where('id', user.user_type_id).first()
                 user.userType = usertype
                 if (usertype.name != 'Admin') {
                   await auth.logout()
                   return response.status(401).json({
                     'message': 'Your are not authenticate user!!'
                   })
                 }
             } catch (error) {
                await auth.logout()
                return response.status(403).json({
                    message:"You are not authenticate user"
                })
             }


            let totalCampain = await Database.table('campaigns').count('campaigns.id as total')
            let totalSubscriber = await Database.table('campaign_subscribers').countDistinct('campaign_subscribers.user_id as total')
            let totalSubscribe = await Database.table('campaign_subscribers').count('campaign_subscribers.user_id as total')
            let campaign = await Campaign.query().orderBy('id','desc').paginate(1,10)
         return {
           totalCampain,
           totalSubscriber,
           totalSubscribe,
           campaign,
         }

    }
    async getAllCampaignSubscriber ({ request, response, view,auth }) {
        let user
        try {
            user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            if (usertype.name != 'Admin') {
                await auth.logout()
                return response.status(401).json({
                'message': 'Your are not authenticate user!!'
            })
            }
          } catch (error) {
              await auth.logout()
            return response.status(403).json({
                message: "You are not authenticate user"
            })
        }
        let data = request.all()
        
        let page = request.input('page') ? request.input('page') : 1
        let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
        let order = request.input('order') ? request.input('order') : 'desc'
        let colName = request.input('colName') ? request.input('colName') : 'id'
        let str = request.input('str') ? request.input('str') : ''
        
        let q = CampaignSubscriber.query().with('user').with('campaign').where('campaign_id', data.campaignId)
        if(str){
            q.whereHas('user', (builder) => {
               builder.where('firstname', 'LIKE', '%' + str + '%')
               
            })
            q.orWhereHas('user', (builder) => {
                 builder.where('lastname', 'LIKE', '%' + str + '%')
                  
            })
        }
        if (colName == 'userName') {
            q.whereHas('user', (builder) => {
              builder.orderBy(colName, order)
            })

        }
        else q.orderBy(colName, order)
   
        
        return await q.paginate(page, pageSize)
        
        
    }
    async addnewCampaign ({ request, response, view,auth }) {
         let user
         try {
           user = await auth.getUser()
           let usertype = await UserType.query().where('id', user.user_type_id).first()
           user.userType = usertype
           if (usertype.name != 'Admin') {
             await auth.logout()
             return response.status(401).json({
               'message': 'Your are not authenticate user!!'
             })
           }
         } catch (error) {
           await auth.logout()
           return response.status(403).json({
             message: "You are not authenticate user"
           })
         }
         let data = request.all()

         let ob ={
             campaignName: data.campaignName,
             campaignDetails: data.campaignDetails,
             campaignDetails: data.campaignDetails,
             campaignDuration: data.campaignDuration,
             campaignBudget: data.campaignBudget,
         }

         return await Campaign.create(ob)

    }
    async updateCampaign ({ request, response, view,auth }) {
         let user
         try {
           user = await auth.getUser()
           let usertype = await UserType.query().where('id', user.user_type_id).first()
           user.userType = usertype
           if (usertype.name != 'Admin') {
             await auth.logout()
             return response.status(401).json({
               'message': 'Your are not authenticate user!!'
             })
           }
         } catch (error) {
           await auth.logout()
           return response.status(403).json({
             message: "You are not authenticate user"
           })
         }
         let data = request.all()

         let ob ={
             campaignName: data.campaignName,
             campaignDetails: data.campaignDetails,
             campaignDetails: data.campaignDetails,
             campaignDuration: data.campaignDuration,
             campaignBudget: data.campaignBudget,
         }

        await Campaign.query().where('id',data.id).update(ob)
         return  await Campaign.query().where('id',data.id).first()
    }
    async deleteCampaign ({ request, response, view,auth }) {
         let user
         try {
           user = await auth.getUser()
           let usertype = await UserType.query().where('id', user.user_type_id).first()
           user.userType = usertype
           if (usertype.name != 'Admin') {
             await auth.logout()
             return response.status(401).json({
               'message': 'Your are not authenticate user!!'
             })
           }
         } catch (error) {
           await auth.logout()
           return response.status(403).json({
             message: "You are not authenticate user"
           })
         }
         let data = request.all()
    let check = await CampaignSubscriber.query().where('campaign_id', data.id).first()

    if (check){
         return response.status(401).json({
           'message': ' you delete this, all subscriber under this Campain will deteted!!'
         })
    }

       return await Campaign.query().where('id',data.id).delete()
    }
    async deleteSubcriber ({ request, response, view,auth }) {
         let user
         try {
           user = await auth.getUser()
           let usertype = await UserType.query().where('id', user.user_type_id).first()
           user.userType = usertype
           if (usertype.name != 'Admin') {
             await auth.logout()
             return response.status(401).json({
               'message': 'Your are not authenticate user!!'
             })
           }
         } catch (error) {
           await auth.logout()
           return response.status(403).json({
             message: "You are not authenticate user"
           })
         }
         let data = request.all()
    let check = await CampaignSubscriber.query().where('campaign_id', data.id).first()

    if (check){
         return response.status(401).json({
           'message': 'You Can not delete this, because Campain has subscriber!!'
         })
    }

       return await CampaignSubscriber.query().where('id', data.id).delete()
    }


}

module.exports = ScampainController
