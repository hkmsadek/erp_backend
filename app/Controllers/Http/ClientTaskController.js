'use strict'
const Task = use('App/Models/Task')
const Database = use('Database')
const RequestType = use('App/Models/RequestType')
const Request= use('App/Models/Request')
const UserBalance = use('App/Models/UserBalance')
const User = use('App/Models/User')
class ClientTaskController {
    async all_task_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page');
        let pageSize = request.input('pageSize');
        let status = request.input('status');
        let searchKey = request.input('searchFilter');
        let query = Task.query()
    
        if(status=='Processing'){
          query.where('status', status)
        }
        else if (status == 'Pending') {
          query.where('status', status)
        }
    
        let res
        if(searchKey){
          res = await query
          .with('user')
          .with('project')
          .where((builder) => {
            builder.whereIn('project_id', (builder) => {
                builder.select('project_id')
                    .from('project_employees')
                    .where('user_id', user.id)
            })
          })
          .whereIn('id', (builder) => {
            builder.select('id')
                .from('tasks')
                .where(Database.raw(`description LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`duration LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`status LIKE "%${searchKey}%"`))  
            })
            .orderBy('id','desc')
            .where('isDeleted',0)
            .paginate(page,pageSize)
        }
        else{
          res = await query
          .with('user')
          .with('project')
          .where((builder) => {
            builder.whereIn('project_id', (builder) => {
                builder.select('project_id')
                    .from('project_employees')
                    .where('user_id', user.id)
            })
          })
          .orderBy('id','desc')
          .where('isDeleted',0)
          .paginate(page,pageSize)
        }
    
        return res;
      }
    
      async taskStatus({request,response}){
        let data = request.all()
        let taskId = request.input('taskId');
        return await Task.query().where('id',taskId).update({
            status: data.status
        })
      }
    
      async pending_task_count_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        return await Task.query()
                    .where((builder) => {
                      builder.whereIn('project_id', (builder) => {
                          builder.select('project_id')
                              .from('project_employees')
                              .where('user_id', user.id)
                      })
                    })
                    .where('isDeleted',0)
                    .where('status','Pending')
                    .getCount()
      }
    
      async task_delete_tech({request,response,params}){
        let data = request.all()
          return await Task.query()
                      .where('id',data.id)
                      .update({
                        isDeleted:1
                      })
    
      }

    //   All Request 
    async all_request_type_tech({request,response}){
        return await RequestType.all();
    }

    async timeExtend_req_tech({request,response}){
        let reqData = request.all()
        let res= await Request.create(reqData)
        if(res){
        return Request.query()
                        .with('user')
                        .with('project')
                        .with('request_type')
                        .orderBy('id','desc')
                        .first()
        }
        return;
    }

    async all_req_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page');
        let pageSize  = request.input('pageSize');
        let req_type = request.input('req_type');
        let searchKey = request.input('searchFilter');
        let query = Request.query()
    
        if(req_type=='Sent'){
            query.where('user_id', user.id)
        }
        else{
            query.where('receiver_id', user.id)
        }

        let res 
        if(searchKey){
            res = await query
            .with('user')
            .with('project')
            .with('request_type')
            .whereIn('id', (builder) => {
            builder.select('id')
                .from('requests')
                .orWhere(Database.raw(`description LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`status LIKE "%${searchKey}%"`))
            })
            .where('isDeleted',0)
            .orderBy('id','desc')
            .paginate(page,pageSize)
        }
        else{
            res = await query
            .with('user')
            .with('project')
            .with('request_type')
            .where('isDeleted',0)
            .orderBy('id','desc')
            .paginate(page,pageSize)
        }
        return res;
    }

    async request_delete_tech({request,response,params}){
        let data = request.all()
        return await Request.query()
                    .where('id',data.id)
                    .update({
                        isDeleted:1
                    })

    }
}

module.exports = ClientTaskController
