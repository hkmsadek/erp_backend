'use strict'
const Task = use('App/Models/Task')
const Database = use('Database')
class TechTaskController {

  async all_task_tech({request,response,auth}){
    let user = {}
    try {
        user = await auth.getUser()
    } catch (error) {
        return response.status(403).json({
            message: 'You are not authorized!'
        })
    }
    let page = request.input('page');
    let pageSize = request.input('pageSize');
    let status = request.input('status');
    let searchKey = request.input('searchFilter');
    let query = Task.query()

    if(status=='Processing'){
      query.where('status', status)
    }
    else if (status == 'Pending') {
      query.where('status', status)
    }

    let res
    if(searchKey){
      res = await query
      .with('user')
      .with('project')
      .where((builder) => {
        builder.whereIn('project_id', (builder) => {
            builder.select('project_id')
                .from('project_employees')
                .where('user_id', user.id)
        })
      })
      .whereIn('id', (builder) => {
        builder.select('id')
            .from('tasks')
            .where(Database.raw(`description LIKE "%${searchKey}%"`))
            .orWhere(Database.raw(`duration LIKE "%${searchKey}%"`))
            .orWhere(Database.raw(`status LIKE "%${searchKey}%"`))  
        })
        .orderBy('id','desc')
        .where('isDeleted',0)
        .paginate(page,pageSize)
    }
    else{
      res = await query
      .with('user')
      .with('project')
      .where((builder) => {
        builder.whereIn('project_id', (builder) => {
            builder.select('project_id')
                .from('project_employees')
                .where('user_id', user.id)
        })
      })
      .orderBy('id','desc')
      .where('isDeleted',0)
      .paginate(page,pageSize)
    }

    return res;
  }

  async taskStatus({request,response}){
    let data = request.all()
    let taskId = request.input('taskId');
    return await Task.query().where('id',taskId).update({
        status: data.status
    })
  }

  async pending_task_count_tech({request,response,auth}){
    let user = {}
    try {
        user = await auth.getUser()
    } catch (error) {
        return response.status(403).json({
            message: 'You are not authorized!'
        })
    }
    return await Task.query()
                .where((builder) => {
                  builder.whereIn('project_id', (builder) => {
                      builder.select('project_id')
                          .from('project_employees')
                          .where('user_id', user.id)
                  })
                })
                .where('isDeleted',0)
                .where('status','Pending')
                .getCount()
  }

  async task_delete_tech({request,response,params}){
    let data = request.all()
      return await Task.query()
                  .where('id',data.id)
                  .update({
                    isDeleted:1
                  })

  }

}

module.exports = TechTaskController
