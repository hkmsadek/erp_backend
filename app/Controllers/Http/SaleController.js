'use strict'
const Project = use('App/Models/Project');
const ProjectEmployee = use('App/Models/ProjectEmployee');
const VideoRequest = use('App/Models/VideoRequest');
const Campaign = use('App/Models/Campaign')
const CampaignSubscriber = use('App/Models/CampaignSubscriber')
const Attachment = use('App/Models/Attachment')
const ProjectTimeline = use('App/Models/ProjectTimeline')
const User = use('App/Models/User')
const UserBalance = use('App/Models/UserBalance')
const ClientAssignedAgent = use('App/Models/ClientAssignedAgent')
const UserType = use('App/Models/UserType')
const Tutorial = use("App/Models/Tutorial");
const Database = use('Database');
class SaleController {

    // Login Routes
    async login({request, response, params, auth}){
        const data = request.all();
        let c = await User.query().where('email',data.email).getCount();
        if(c == 0){
          return response.status(401).json({
            'success': false,
            'message': 'email does not exists!'
            
          });
        }
        c = await User.query().where('email',data.email).where('user_type_id',1).getCount();
        if(c == 0){
          return response.status(401).json({
            'success': false,
            'message': 'You are not authorized'
            
          });
        }
    
    
        try {
          let user = await auth.query().attempt(data.email, data.password)  
          return user;
        } catch (e) {
            return response.status(401).json({
                'message': 'Invalid email or password. Please try again.'
            })
        }
    }
    async logout({request, response, params, auth}){
        await auth.logout()
        return 'logged out'
    }
    async initdata ({ request, response, auth }) {
        try {
          const user = await auth.getUser()
         // console.log(user)
          return {
            user: user
          }
        } catch (error) {
          console.log(error.message)
          return false
        }
    }

    // Clients
    async clientList({ request, response,auth}){
        let user;
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page') ? request.input('page') : 1;
        let total = request.input('total') ? request.input('total') : 5;
        let str = request.input('str');
        let data =  Project.query().with('client').with('agents.user').whereHas('agents', (builder) => { builder.where('user_id',user.id);});
        if(str) {

            data.where((builder)=>{
                builder.whereHas('client' ,(builder2)=>{
                    builder2.where('firstname', 'like', '%'+str+'%');
                });
                builder.orWhere('name', 'like', '%'+str+'%');
            });
        } 
        let finalData = await  data.orderBy('status','asc').paginate(page,total);
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }

    async projectStatistics({ request, response,auth,params}){
        let user;
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                'success': false,
                message: 'You are not authorized!'
            })
        }
        let salesIncluded = await ProjectEmployee.query().where('project_id',params.id).where('user_id',user.id).getCount();
        if(salesIncluded == 0){
            return response.status(401).json({
                'success': false,
                message: 'You are not authorized in this project details!'
            })
        }
        let data =  Project.query().with('client').with('agents.user').whereHas('agents', (builder) => { builder.where('user_id',user.id);});
        
        let finalData = await  data.orderBy('status','asc').paginate(page,total);
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }

    // Apointments
    async appintments({ request, response,auth}){
        let user;
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page') ? request.input('page') : 1;
        let total = request.input('total') ? request.input('total') : 5;
        let type = request.input('type') ? request.input('type') : 'receiver';
        let str = request.input('str');
        let data =  VideoRequest.query().with('project').with('sender');
        if(type== 'receiver') data.where('receiver_id',user.id);
        if(type== 'sender') data.where('user_id',user.id);
        if(type== 'all') {
            data.where((builder)=>{
                builder.where('user_id',user.id);
                builder.orWhere('receiver_id',user.id);
            });
        }
        if(str) {

            data.where((builder)=>{
                builder.whereHas('client' ,(builder2)=>{
                    builder2.where('firstname', 'like', '%'+str+'%');
                });
                builder.orWhere('name', 'like', '%'+str+'%');
            });
        } 
        let finalData = await  data.orderBy('date','desc').paginate(page,total);
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }
    async editAppointmentStatus({ request, response,auth}){
        let data = request.all();
        let user;
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(401).json({
                'success': false,
                message: 'You are not authorized!'
            })
        }
        
        let finalData = await  VideoRequest.query().where('id',data.id).update(data);
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }
    async addAppointment({ request, response,auth}){
        let data = request.all();
        let user = await auth.getUser();
        data.user_id = user.id;
        
        let rawData = await  VideoRequest.create(data);
        let finalData = await VideoRequest.query().with('project').with('sender').where('id',rawData.id).first();
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }


    // Query  Methods
    async getClientList({ request, response,auth}){
        let user;
        try {
            user = await auth.getUser()
            
        } catch (error) {
            return response.status(401).json({
                message: 'You are not authorized!'
            })
        }
        let str = request.input('str');
        let projects = await  ProjectEmployee.query().where('user_id',user.id).pluck('project_id');
        let data = Database
        .select('projects.*','users.firstname','users.lastname')
        .from('projects')
        .innerJoin('users', 'projects.user_id', 'users.id')
        data.whereIn('projects.id',projects);
        
        // let data =  VideoRequest.query().with('project').with('sender').where('receiver_id',user.id);
        if(str){
            data.where((builder) =>{
              builder.where('users.firstName', 'like', '%'+str+'%');
              builder.orWhere('users.lastName', 'like', '%'+str+'%');
              builder.orWhere('projects.name', 'like', '%'+str+'%');
              builder.orWhere('projects.user_id',str);
            });
        }  
        let finalData = await  data.limit(5);
        // return data
        return response.status(200).json({
          'success': true,
          'data': finalData,
        });
    }

    // Campaine Methods
    
    async getALlCampaignSummary ({ request, response, view,auth }) {
        let user = await auth.getUser();
        let page = request.input('page') ? request.input('page') : 1;
        let total = request.input('total') ? request.input('total') : 5;
        let str = request.input('str');
        let type = request.input('type')? request.input('type') : 1;
        let data =  Campaign.query();
        if(type==1) data.withCount('subscriber', (builder) => { builder.where('user_id',user.id);});
        if(type==2) data.whereHas('subscriber', (builder) => { builder.where('user_id',user.id);});
        
        if(str) {

            data.where((builder)=>{
                builder.where('campaignName', 'LIKE', '%' + str + '%')
                .orWhere('campaignBudget', 'LIKE', '%' + str + '%')
                .orWhere('campaignDuration', 'LIKE', '%' + str + '%')
            });
        } 
        let finalData = await  data.orderBy('id','desc').paginate(page,total);
        return response.status(200).json({
            'success': true,
            'campaign': finalData,
        });

    }
    
    async addnewCampaignSubcriber ({ request, response, view,auth }) {
         let  user = await auth.getUser()
         
         let data = request.all();
         data.user_id = user.id;
         let finalData = await CampaignSubscriber.create(data);

         return response.status(200).json({
            'success': true,
            'data': finalData,
        });

    }

    // Project
    async all_project_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        let page = request.input('page');
        let pageSize = request.input('pageSize');
        let status = request.input('status');
        let searchKey = request.input('searchFilter');
        let query = Project.query()
    
        if(status=='Processing'){
          query.where('status', status)
        }
        else if (status == 'Pending') {
          query.where('status', status)
        }
    
        let res 
        if(searchKey){
          res = await query
          .with('user')
          .withCount('task as total_task')
          .where((builder) => {
            builder.whereIn('id', (builder) => {
                builder.select('project_id')
                    .from('project_employees')
                    .where('user_id', user.id)
            })
          })
          .whereIn('id', (builder) => {
            builder.select('id')
                .from('projects')
                .where(Database.raw(`name LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`description LIKE "%${searchKey}%"`))
                .orWhere(Database.raw(`tech_price LIKE "%${searchKey}%"`))
            })
          .where('isDeleted',0)
          .orderBy('id','desc')
          .paginate(page,pageSize)
        } 
        else{
          res = await query
          .with('user')
          .withCount('task as total_task')
          .withCount('task as completed_task', (builder) => {
            builder.where('status','Completed')
          })
          .where((builder) => {
            builder.whereIn('id', (builder) => {
                builder.select('project_id')
                    .from('project_employees')
                    .where('user_id', user.id)
            })
          })
          .where('isDeleted',0)
          .orderBy('id','desc')
          .paginate(page,pageSize)
        }
    
         return res; 
      }
    
      async all_project_doc_tech({request,response}){
            return await Attachment.query()
                        .with('user')
                        .with('project')
                        .where('project_id',request.input('project_id'))
                        .orderBy('id','desc')
                        .fetch()
    
      }
    
      async projectStatus({request,response}){
        let data = request.all()
        let projcetId = request.input('projcetId');
        if( data.status=='Processing'){
          const now = new Date();
          await Project.query().where('id', projcetId).update({
              status: data.status,
              startDate: now,
          })
          return Project.query().where('id', projcetId).first()
        }
        else{
          return await Project.query().where('id', projcetId).update({
            status: data.status
          })
        }
      }
    
      async pending_project_count_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        return await Project.query()
                    .where((builder) => {
                      builder.whereIn('id', (builder) => {
                          builder.select('project_id')
                              .from('project_employees')
                              .where('user_id',user.id)
                      })
                    })
                    .where('isDeleted',0)
                    .where('status','Pending')
                    .getCount()
      }
    
      async get_all_project_tech({request,response,auth}){
        let user = {}
        try {
            user = await auth.getUser()
        } catch (error) {
            return response.status(403).json({
                message: 'You are not authorized!'
            })
        }
        return await Project.query()
                            .where((builder) => {
                              builder.whereIn('id', (builder) => {
                                  builder.select('project_id')
                                      .from('project_employees')
                                      .where('user_id',user.id)
                              })
                            })
                            .where('status','!=','pending')
                            .where('isDeleted',0)
                            .orderBy('id','desc')
                            .fetch()
    
      }
    
    
      async all_project_timeline_tech({request,response}){
              return await ProjectTimeline.query()
                    .with('task')
                    .with('project')
                    .where('project_id',request.input('project_id'))
                    .orderBy('id','desc')
                    .fetch()
    
      }
    
      async running_project_count_tech({request,response,auth}){
              let user = {}
              try {
                  user = await auth.getUser()
              } catch (error) {
                  return response.status(403).json({
                      message: 'You are not authorized!'
                  })
              }
              return await Project.query()
                    .where((builder) => {
                      builder.whereIn('id', (builder) => {
                          builder.select('project_id')
                              .from('project_employees')
                              .where('user_id',user.id)
                      })
                    })
                    .where('isDeleted',0)
                    .where('status','Processing')
                    .getCount()
        }
    
        async get_single_project({request,response,params}){
            return await Project.query()
                        .with('user')
                        .withCount('question as total_question')
                        .where('id',request.input('project_id'))
                        .orderBy('id','desc')
                        .first() 
    
        }
    
        async update_project_tech({request,response,params}){
          let data = request.all()
            return await Project.query()
                        .where('id',data.id)
                        .update({
                          tech_price:data.tech_price,
                          hour:data.hour,
                        })
                        
    
        }
    
        async project_delete_tech({request,response,params}){
          let data = request.all()
            return await Project.query()
                        .where('id',data.id)
                        .update({
                          isDeleted:1
                        })
    
        }
    
    // User Balance
    async allagentbalance ({ request, response, view,auth  }) {
        let user = await auth.getUser()

        let data = request.all()
        let page = request.input('page') ? request.input('page') : 1
        let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
        let str = request.input('str') ? request.input('str') : ''
        let order = request.input('order') ? request.input('order') : 'desc'
        let colName = request.input('colName') ? request.input('colName') : 'id'

        let  q  =  UserBalance.query().with('project').where('user_id', user.id)
        if(str){

            q.whereHas('project', (builder) => {
            builder.where('name', 'LIKE', '%' + str + '%')
            })
        }
      
        let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
        return alldata


   }


   async getAgentBalance ({ request, response, view ,auth }) {

        let user = await auth.getUser()
        const alldata = await  UserBalance.query().where('user_id', user.id).getSumDistinct('amount')
        return alldata

   }


    //  Dashbaord Overview   
   async dashboardOverview ({ request, response, view ,auth }) { 

        let user = await auth.getUser()
        const totalClients  = await ClientAssignedAgent.query().where('agent_id',user.id).getCount();
        const clients  = await ClientAssignedAgent.query().with('client').where('agent_id',user.id).limit(5).fetch();
        const totalProjects  = await ProjectEmployee.query().where('user_id',user.id).getCount();
        const projects = await Project.query().with('user').withCount('task as total_task').where((builder) => { builder.whereIn('id', (builder) => { builder.select('project_id').from('project_employees').where('user_id', user.id)})}).where('isDeleted',0).limit(5).fetch()
        const runningProjects = await Project.query().where('status','Processing').with('user').withCount('task as total_task').where((builder) => { builder.whereIn('id', (builder) => { builder.select('project_id').from('project_employees').where('user_id', user.id)})}).where('isDeleted',0).getCount()
        const totalBalance = await  UserBalance.query().where('user_id', user.id).getSumDistinct('amount')

        return response.status(200).json({
            'success': true,
            'totalClients': totalClients,
            'clients': clients,
            'totalProjects': totalProjects,
            'runningProjects': runningProjects,
            'projects': projects,
            'totalBalance': totalBalance,
          });

   }
    

    // Tutor Methods
    async showInstructionTutorials({ request, response, auth }) {

        let user;
        try {
          user = await auth.getUser()
        } catch (error) {
          return response.statu(401).json({
            message: 'You are not authorized!'
          })
        }
    
        let page = request.input('page') ? Number(request.input('page')) : 1
        let pageSize = request.input('pageSize') ? Number(request.input('pageSize')) : 10
    
        return Tutorial.query()
          .paginate(page, pageSize)
    
    
      }
      async showInstructionTutorialsById({ request, response, params, auth }) {
        let user;
        try {
          user = await auth.getUser()
        } catch (error) {
          return response.statu(401).json({
            message: 'You are not authorized!'
          })
        }
    
        if (!params.id) {
          return response.statu(403).json({
            message: 'Invalid Request!'
          })
        }
    
        let page = request.input('page') ? Number(request.input('page')) : 1
        let pageSize = request.input('pageSize') ? Number(request.input('pageSize')) : 10
    
        return Tutorial.query()
          .where('tutorial_category_id', params.id)
          .paginate(page, pageSize)
    
    
      }    
    // TEST Method
    async test({ request, response,auth}){
        let data = Database
          .select('users.*','user_types.name as userType')
          .from('users')
          .innerJoin('user_types', 'user_types.id', 'users.user_type_id')
        //   .leftJoin('users', 'user_balances.club_id', 'clubs.id')
        // if (club_id) {
        //   data.where('clubplayers.club_id', club_id)
        // }
        // if (str) {
        //   data.where((builder) => {
        //     builder.where('users.firstName', 'like', '%' + str + '%');
        //     builder.orWhere('users.lastName', 'like', '%' + str + '%');
        //     builder.orWhere('clubplayers.user_id', str);
        //   });
        // }
        const clubplayers = await data.paginate(1, 10);
        return clubplayers;
    }
}

module.exports = SaleController
