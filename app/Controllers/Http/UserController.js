'use strict'
const User = use('App/Models/User')
const UserType = use('App/Models/UserType')
class UserController {
   async user({request, response, params, auth}){
       try {
          const user = await auth.loginViaId(34)

       } catch (error) {
          return error
       }

   }

  async getUser({request, response, params, auth}){
      try {
        const user = await auth.getUser()
        console.log('cookie is.. haha', request.headers())

        return user
      } catch (error) {
          //console.log('error is', error)
          return 'not logged in ... '
      }

  }
  async logout({request, response, params, auth}){
      await auth.logout()
      return 'logged out'
  }
    async login({ request, response, auth }) {
        const data = request.all()
        try {
            let user = await auth.query().attempt(data.email, data.password)
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            if(usertype.name!='Admin'){
                   await auth.logout()
                      return response.status(401).json({
                        'message': 'Your are not authenticate user!!'
                      })
            }
            
            user.userType = usertype
            

            return response.status(200).json(user)
        } catch (e) {
            // console.log(e.message)
            return response.status(401).json(
                {
                    'message': 'Invalid email or password. Please try again.'
                }
            )
        }
    }

    async initdata({ request, response, auth }) {
        try {
            let user = await auth.getUser()
            let usertype = await UserType.query().where('id', user.user_type_id).first()
            user.userType = usertype
            return user
        } catch (error) {
            return false
        }
    }

        async storeUser ({ request, response }) {  
    
        let reqData = request.all()
        const rules = {
            email: 'required|email|unique:users',
            password: 'required',
            username: 'required',
        }
        const validation = await validate(request.all(), rules)
    
        if (validation.fails()) {
            return response.status(401).json(validation.messages())
        }
        return await User.create(reqData) 
      }


}

module.exports = UserController
