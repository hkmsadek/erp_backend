'use strict'
const User = use("App/Models/User");
const Project = use('App/Models/Project')
const { validate } = use('Validator')
const Attachment = use('App/Models/Attachment')
const Helpers = use('Helpers')
const Env = use('Env')

class ClientController {

async user({ request, response, params, auth }) {
    try {
      const user = await auth.loginViaId(34);
    } catch (error) {
      return error;
    }
  }

  async getUser({ request, response, params, auth }) {
    try {
      const user = await auth.getUser();
      // console.log("cookie is.. haha", request.headers());

      return user;
    } catch (error) {
      //console.log('error is', error)
      return false;
    }
  }

  async clinet_logout({ request, response, params, auth }) {
    await auth.logout();
    return "logged out";
  }

  async showSalesReps({ request, response }) {

    let data = await User.query().where('user_types_id', 1).fetch()
    data.toJSON()
    return data
  }

  async clientRegister({ request, response, auth }) {
    const rules = {
      email: 'required|email|unique:users,email',
      password: 'required'
    }
    const messages = {
      'email.required': 'Email is required',
      'email.unique': 'Email already in use',
    }

    const data = {
      firstname: request.body.firstname ? request.body.firstname : "",
      lastname: request.body.lastname ? request.body.lastname : "",
      email: request.body.email ? request.body.email : "",
      password: request.body.password ? request.body.password : "",
      address: request.body.address ? request.body.address : "",
      phone: request.body.phone ? request.body.phone : "",
      postcode: request.body.postcode ? request.body.postcode : "",
      city: request.body.city ? request.body.city : "",
      country: request.body.country ? request.body.country : "",
      user_type_id: 2
    }
    const validation = await validate(data, rules, messages)
    if (validation.fails()) {
      return response.status(401).json(validation.messages())
    }
    let user = await User.create(data)
    user = user.toJSON()
    console.log('user is', user)
    return response.status(201).json({
      user: user,
    });
  }


  async clientLogin({ request, response, auth }) {
    const data = request.all();

    try {
      // partnerUser, partnerRepresentative
      // let user = await auth.attempt(data.email, data.password);
      let user = await User.query().where('email', request.body.email).first()
      if (user) {

        if (user.user_type_id == 3 || user.user_type_id == 2) {
          user = await auth.attempt(data.email, data.password);
          return user;
        } else {
          return response.status(401).json({
            message: "No client account registered to this email",
          });
        }
      }
    } catch (e) {
      return response.status(401).json({
        message: "Invalid email or password. Please try again.",
      });
    }
  }

    // Tutorial
    async showInstructionTutorials({ request, response, auth }) {

        let user;
        try {
          user = await auth.getUser()
        } catch (error) {
          return response.statu(401).json({
            message: 'You are not authorized!'
          })
        }
    
        let page = request.input('page') ? Number(request.input('page')) : 1
        let pageSize = request.input('pageSize') ? Number(request.input('pageSize')) : 10
    
        return Tutorial.query()
          .paginate(page, pageSize)
    
    
      }
      async showInstructionTutorialsById({ request, response, params, auth }) {
        let user;
        try {
          user = await auth.getUser()
        } catch (error) {
          return response.statu(401).json({
            message: 'You are not authorized!'
          })
        }
    
        if (!params.id) {
          return response.statu(403).json({
            message: 'Invalid Request!'
          })
        }
    
        let page = request.input('page') ? Number(request.input('page')) : 1
        let pageSize = request.input('pageSize') ? Number(request.input('pageSize')) : 10
    
        return Tutorial.query()
          .where('tutorial_category_id', params.id)
          .paginate(page, pageSize)
    
    
      }   

    // Dashboard
    async get_dashboad_project({ request, response, params, auth }) {
        let user = {}
        try {
          user = await auth.getUser()
        } catch (error) {
          return response.status(403).json({
            message: 'You are not authorized!'
          })
        }
        return await Project.query()
          .with('user')
          .withCount('task as total_task')
          .withCount('task as completed_task', (builder) => {
            builder.where('status', 'Completed')
          })
          .where((builder) => {
            builder.whereIn('id', (builder) => {
              builder.select('project_id')
                .from('project_employees')
                .where('user_id', user.id)
            })
          })
          .where('isDeleted', 0)
          .limit(5)
          .orderBy('id', 'desc')
          .fetch()
    
      }

    //   upload

    async projectSubmitAttachmentUpload({ request, response, auth }) {
        // let user
        // try {
        //     user = await auth.getUser()
        // } catch (error) {
        //     console.log(error)
        //     return response.status(401).json({
        //         message: 'You are not authorized!'
        //     })
        // }
        const file = request.file('file', {
            allowedExtensions: ['jpg', 'png', 'doc', 'pdf', 'docx', 'txt'],
            maxSize: '20mb'
        })

        if (file) {
            const type = file.toJSON().extname;
            const name = `file_${new Date().getTime()}.${type}`

            // UPLOAD THE file TO UPLOAD FOLDER
            await file.move(Helpers.publicPath('uploads'), {
                name: name
            })

            if (!file.moved()) {
                console.log('upload error')
                return file.error()
            }

            await Attachment.create({
                
            })

            return response.status(200).json({
                message: 'File has been uploaded successfully!',
                url: `${Env.get('SITE_URL')}/uploads/${name}`,
                fileExt: type,
            })
        }

        return response.status(200).json({
            message: 'Invalid Request!'
        })
    }
}
module.exports = ClientController
