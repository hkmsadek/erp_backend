'use strict'
// helpers
const Database = use('Database')
const Ws = use('Ws')
module.exports = class NotificationClass {

    // chat notificaiton
    sendChatWSNotificaiton(conversation_id, message, receiver_id, chat_id, user, lastMessages) {
        // lastMessages = (lastMessages && lastMessages.length > 0) ? lastMessages.toJSON() : []
        const meta = {
            input: {
                conversation_id: conversation_id,
                message: message,
                receiver_id: receiver_id,
                chat_id: chat_id,
                created_at: new Date(),
                lastMessages: [],
                userName: user.userName,
                firstName: user.firstName,
                lastName: user.lastName,
                profilePic: user.profilePic,
                user_id: user.id
            },
            type: 'chat-sent'

        }
        const chat = Ws.getChannel('notification:*')
        const p = chat.topic(`notification:${receiver_id}`)
        if (p) {
            console.log('socket is called for', receiver_id)
            p.broadcastToAll('message', meta)
        }
    }

}

