'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Campaign extends Model {
       subscriber() {
        return this.hasMany('App/Models/CampaignSubscriber', 'id', 'campaign_id')
       }
}

module.exports = Campaign
