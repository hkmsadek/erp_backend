'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tutorial extends Model {

      category() {
        return this.belongsTo('App/Models/TutorialCategory', 'tutorial_category_id', 'id')
      }
}

module.exports = Tutorial
