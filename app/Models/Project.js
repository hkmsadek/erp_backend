'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Project extends Model {
      details() {
        return this.hasMany('App/Models/Task', 'id', 'project_id')
      }
      user() {
        return this.belongsTo('App/Models/User', 'user_id', 'id')
      }
      question() {
        return this.hasMany('App/Models/Question')
      }
      task() {
        return this.hasMany('App/Models/Task')
      }
      client () {
        return this.belongsTo('App/Models/User', 'user_id')
      }
      agents () {
          return this.hasMany('App/Models/ProjectEmployee','id', 'project_id')
      }



}

module.exports = Project
