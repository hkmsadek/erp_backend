'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserBalance extends Model {
       user() {
         return this.belongsTo('App/Models/User', 'user_id', 'id')
       }
       project() {
        return this.belongsTo('App/Models/Project').select('id','name')
    }
    
}

module.exports = UserBalance
