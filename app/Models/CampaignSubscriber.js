'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CampaignSubscriber extends Model {
      user() {
        return this.belongsTo('App/Models/User', 'user_id', 'id')
      }
      campaign() {
        return this.belongsTo('App/Models/Campaign', 'campaign_id', 'id')
      }

}

module.exports = CampaignSubscriber
