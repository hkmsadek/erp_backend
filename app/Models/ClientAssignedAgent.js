'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ClientAssignedAgent extends Model {
    client () {
        return this.belongsTo('App/Models/User', 'client_id')
    }
}

module.exports = ClientAssignedAgent
