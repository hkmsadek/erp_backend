'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class SalesRepresentative {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request 
   * @param {Function} next
   */
  async handle ({ request , auth ,response}, next) {
    // call next to advance the request
    try {
      let user = await auth.getUser();
      if(user.user_type_id !== 1) 
        return response.status(401).json({
          message: 'You are not authorized!'
        })

    } catch (error) {
      return response.status(401).json({
          message: 'You are not authorized !'
      })
    }
    await next();
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async wsHandle ({ request }, next) {
    // call next to advance the request
    await next()
  }
}

module.exports = SalesRepresentative
